CLANG=clang++
GCC=/usr/local/bin/g++-8
CXXFLAGS=-std=c++17 -fexceptions # -Wall -Wextra -pedantic 
CLANGFLAGS=$(shell llvm-config --cxxflags)
GCCFLAGS=-I$(shell llvm-config --includedir) -I$(shell brew --cellar llvm)/7.0.0/include/c++/v1 -nostdinc++ -nodefaultlibs -lc++ -lc++abi -lm -lc -lgcc
LLVMFLAGS=$(shell llvm-config --ldflags --system-libs --libs all)

all:
	$(CLANG) $(LLVMFLAGS) $(CLANGFLAGS) $(CXXFLAGS) src/main.cxx -o bin/finchc

gcc:
	$(GCC) $(LLVMFLAGS) $(GCCFLAGS) $(CXXFLAGS) src/main.cxx -o bin/finchc

debug:
	$(CLANG) $(LLVMFLAGS) $(CLANGFLAGS) $(CXXFLAGS) -O0 -g -fstandalone-debug src/main.cxx -o bin/finchc.debug

clean:
	rm -f bin/*