#include <algorithm>
#include <fstream>
#include <iostream>
#include <optional>
#include <stack>
#include <unordered_map>
#include <vector>

#include <llvm/ADT/APFloat.h>
#include <llvm/ADT/STLExtras.h>
#include <llvm/Analysis/InstructionSimplify.h>
#include <llvm/IR/BasicBlock.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/DerivedTypes.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/LegacyPassManager.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Type.h>
#include <llvm/IR/Verifier.h>
#include <llvm/Support/TargetSelect.h>
#include <llvm/Target/TargetMachine.h>
#include <llvm/Transforms/InstCombine/InstCombine.h>
#include <llvm/Transforms/Scalar.h>
#include <llvm/Transforms/Scalar/GVN.h>
//#include <llvm/Transforms/Utils.h>

namespace finch::backend::codegen
{
    struct item
    {
        llvm::AllocaInst *addr;
    };

    struct compiler_ctx
    {
        llvm::LLVMContext llvm_ctx;
        std::unique_ptr<llvm::Module> llvm_module;
        std::unique_ptr<llvm::legacy::FunctionPassManager> llvm_pass_manager;
        llvm::IRBuilder<> llvm_builder;
        // XXX Hack
        llvm::Function *current_fun;
        // XXX Hack
        std::map<std::string, item> named_values;

        compiler_ctx() : llvm_builder{llvm_ctx} { }

        void init(const std::string &module_name)
        {
            llvm_module = std::make_unique<llvm::Module>(module_name, llvm_ctx);
            llvm_pass_manager = llvm::make_unique<llvm::legacy::FunctionPassManager>(llvm_module.get());

            // Do simple "peephole" optimizations and bit-twiddling optzns.
            llvm_pass_manager->add(llvm::createInstructionCombiningPass());
            // Reassociate expressions.
            llvm_pass_manager->add(llvm::createReassociatePass());
            // Eliminate Common SubExpressions.
            llvm_pass_manager->add(llvm::createGVNPass());
            // Simplify the control flow graph (deleting unreachable blocks, etc).
            llvm_pass_manager->add(llvm::createCFGSimplificationPass());
            // Promote allocas to registers.
            llvm_pass_manager->add(llvm::createPromoteMemoryToRegisterPass());
            // Do simple "peephole" optimizations and bit-twiddling optzns.
            llvm_pass_manager->add(llvm::createInstructionCombiningPass());
            // Reassociate expressions.
            llvm_pass_manager->add(llvm::createReassociatePass());

            llvm_pass_manager->doInitialization();
        }

        llvm::Type *int_ty(size_t bits)
        {
            return llvm::IntegerType::get(llvm_ctx, bits);
        }

        llvm::AllocaInst *entry_block_alloca(llvm::Function *fun, const std::string &name, llvm::Type *ty)
        {
            llvm::IRBuilder<> builder{&fun->getEntryBlock(), fun->getEntryBlock().begin()};
            return builder.CreateAlloca(ty, 0, name.c_str());
        }
    };
}

namespace finch::semant
{
    struct type
    {
        virtual ~type() { }

        virtual llvm::Type *to_llvm_type(backend::codegen::compiler_ctx &ctx) = 0;
    };

    struct int_type : type
    {
        size_t bits;
        bool is_signed;

        int_type(size_t bits, bool is_signed = true) : bits{bits}, is_signed{is_signed} { }

        llvm::Type *to_llvm_type(backend::codegen::compiler_ctx &ctx) override
        {
            return ctx.int_ty(bits);
        }
    };

    std::unique_ptr<type> bool_type()
    {
        return std::make_unique<int_type>(1, false);
    }

    struct item
    {
        std::unique_ptr<type> ty;
        bool mut;
    };

    struct ctx
    {
        std::map<std::string, item> items;
    };
}

namespace finch::hir
{
    struct value
    {
        std::unique_ptr<semant::type> ty;

        value(std::unique_ptr<semant::type> ty) : ty{std::move(ty)} { }

        virtual ~value() { }

        virtual llvm::Value *to_llvm(backend::codegen::compiler_ctx &ctx) = 0;
    };

    struct has_addr
    {
        virtual ~has_addr() { }

        virtual llvm::AllocaInst *addr(backend::codegen::compiler_ctx &ctx) = 0;
    };

    struct instr
    {
        virtual ~instr() { }

        virtual void codegen(backend::codegen::compiler_ctx &ctx) = 0;
    };

    struct int_imm : value
    {
        int64_t int_value;

        int_imm(int64_t int_value, std::unique_ptr<semant::type> ty)
            : value{std::move(ty)}, int_value{int_value}
        { }

        llvm::Value *to_llvm(backend::codegen::compiler_ctx &ctx) override
        {
            return llvm::ConstantInt::get(ty->to_llvm_type(ctx), int_value, reinterpret_cast<semant::int_type *>(ty.get())->is_signed);
        }
    };

    struct mem_loc : value, has_addr
    {
        std::string name;

        mem_loc(const std::string &name) : value{std::make_unique<semant::int_type>(64)}, name{name} { }

        llvm::Value *to_llvm(backend::codegen::compiler_ctx &ctx) override
        {
            return ctx.llvm_builder.CreateLoad(ctx.named_values[name].addr, name);
        }

        llvm::AllocaInst *addr(backend::codegen::compiler_ctx &ctx) override
        {
            return ctx.named_values[name].addr;
        }
    };

    struct store : instr
    {
        std::unique_ptr<has_addr> lhs;
        std::unique_ptr<value> rhs;

        store(std::unique_ptr<has_addr> lhs, std::unique_ptr<value> rhs)
            : lhs{std::move(lhs)}, rhs{std::move(rhs)}
        { }

        void codegen(backend::codegen::compiler_ctx &ctx) override
        {
            auto addr = lhs->addr(ctx);
            auto val = rhs->to_llvm(ctx);
            ctx.llvm_builder.CreateStore(val, addr);
        }
    };

    struct block
    {
        std::unique_ptr<value> condition = nullptr;
        std::vector<std::unique_ptr<instr>> body;

        void add_condition(std::unique_ptr<value> condition)
        {
            this->condition = std::move(condition);
        }

        bool is_conditional() const
        {
            return condition.operator bool();
        }

        void emit(std::unique_ptr<instr> instr)
        {
            body.emplace_back(std::move(instr));
        }

        llvm::BasicBlock *codegen(backend::codegen::compiler_ctx &ctx, const std::string &label)
        {
            auto basic_block = llvm::BasicBlock::Create(ctx.llvm_ctx, label);
            ctx.llvm_builder.SetInsertPoint(basic_block);

            for (auto &&stmt : body) {
                stmt->codegen(ctx);
            }

            return basic_block;
        }
    };

    struct conditional : instr
    {
        std::vector<std::unique_ptr<block>> conditional_blocks;
        std::unique_ptr<block> unconditional_block;

        conditional(std::vector<std::unique_ptr<block>> conditional_blocks, std::unique_ptr<block> unconditional_block)
            : conditional_blocks{std::move(conditional_blocks)}, unconditional_block{std::move(unconditional_block)}
        { }

        void codegen(backend::codegen::compiler_ctx &ctx) override
        {
            auto curr_block = ctx.llvm_builder.GetInsertBlock();
            std::vector<llvm::Value *> conditions; // = {cons.condition->to_llvm(ctx)};
            std::transform(
                conditional_blocks.begin(), conditional_blocks.end(),
                std::back_inserter(conditions),
                [&](std::unique_ptr<block> &b) { return b->condition->to_llvm(ctx); }
            );
            std::vector<llvm::BasicBlock *> blocks; // = {cons.codegen(ctx, "then"), alt.codegen(ctx, "else")};
            std::transform(
                conditional_blocks.begin(), conditional_blocks.end(),
                std::back_inserter(blocks),
                [&](std::unique_ptr<block> &b) { return b->codegen(ctx, "cblock"); }
            );
            blocks.emplace_back(unconditional_block->codegen(ctx, "ublock"));
            auto ifcont_block = llvm::BasicBlock::Create(ctx.llvm_ctx, "ccont");

            for (uint64_t i = 0; i < blocks.size() - 1; i++) {
                ctx.llvm_builder.SetInsertPoint(curr_block);
                ctx.llvm_builder.CreateCondBr(conditions[i], blocks[i], blocks[i + 1]);
                ctx.current_fun->getBasicBlockList().push_back(blocks[i]);
                ctx.llvm_builder.SetInsertPoint(blocks[i]);
                if (i + 1 < blocks.size() - 1) {
                    ctx.llvm_builder.CreateBr(blocks[i + 1]);
                } else {
                    ctx.llvm_builder.CreateBr(ifcont_block);
                }
                curr_block = blocks[i];
            }

            ctx.current_fun->getBasicBlockList().push_back(blocks[blocks.size() - 1]);
            ctx.llvm_builder.SetInsertPoint(blocks[blocks.size() - 1]);
            ctx.llvm_builder.CreateBr(ifcont_block);

            ctx.current_fun->getBasicBlockList().push_back(ifcont_block);
            ctx.llvm_builder.SetInsertPoint(ifcont_block);
        }
    };

    struct ret_val : instr
    {
        std::unique_ptr<value> val;

        ret_val(std::unique_ptr<value> val) : val{std::move(val)} { }

        void codegen(backend::codegen::compiler_ctx &ctx) override
        {
            auto ret_val = val->to_llvm(ctx);
            ctx.llvm_builder.CreateRet(ret_val);
        }
    };

    struct function
    {
        std::string name;
        std::map<std::string, semant::item> locals;
        block body;

        function(const std::string &name) : name{name} { }

        void add_local(const std::string &name, std::unique_ptr<semant::type> ty, bool mut)
        {
            locals[name] = {std::move(ty), mut};
        }

        void codegen(backend::codegen::compiler_ctx &ctx)
        {
            auto fun_ty = llvm::FunctionType::get(ctx.int_ty(64), {}, false);
            auto fun = llvm::Function::Create(fun_ty, llvm::Function::ExternalLinkage, name, ctx.llvm_module.get());

            ctx.current_fun = fun;

            auto basic_block = llvm::BasicBlock::Create(ctx.llvm_ctx, "entry", fun);
            ctx.llvm_builder.SetInsertPoint(basic_block);

            for (auto &&[local_name, item] : locals) {
                auto addr = ctx.entry_block_alloca(ctx.current_fun, local_name, item.ty->to_llvm_type(ctx));
                ctx.named_values[local_name] = {addr};
            }

            auto body_block = body.codegen(ctx, "fun_body");
            fun->getBasicBlockList().insertAfter(basic_block->getIterator(), body_block);
            ctx.llvm_builder.SetInsertPoint(basic_block);
            ctx.llvm_builder.CreateBr(body_block);

            ctx.llvm_module->print(llvm::errs(), nullptr);

            ctx.llvm_pass_manager->run(*fun);

            ctx.current_fun = nullptr;

            llvm::verifyFunction(*fun);
        }
    };
}

namespace finch::frontend::ast
{
    struct expr_node
    {
        virtual ~expr_node() { }

        virtual std::unique_ptr<hir::value> to_hir(semant::ctx &ctx) = 0;
    };

    struct node
    {
        virtual ~node() { }

        virtual std::unique_ptr<hir::instr> to_hir(semant::ctx &ctx, hir::function *fun) = 0;
    };

    struct lhs_node
    {
        virtual ~lhs_node() { }

        virtual bool mut(semant::ctx &ctx) = 0;

        virtual std::unique_ptr<hir::has_addr> to_hir_loc(semant::ctx &ctx) = 0;
    };

    struct int_lit_exp : expr_node
    {
        int64_t value;
        bool is_signed;

        int_lit_exp(int64_t value, bool is_signed) : value{value}, is_signed{is_signed} { }

        std::unique_ptr<hir::value> to_hir(semant::ctx &ctx) override
        {
            return std::make_unique<hir::int_imm>(value, std::make_unique<semant::int_type>(64, is_signed));
        }
    };

    struct bool_lit_exp : expr_node
    {
        bool value;

        bool_lit_exp(bool value) : value{value} { }

        std::unique_ptr<hir::value> to_hir(semant::ctx &ctx) override
        {
            return std::make_unique<hir::int_imm>(value, semant::bool_type());
        }
    };

    struct ident_exp : expr_node, lhs_node
    {
        std::string name;

        ident_exp(const std::string &name) : name{name} { }

        bool mut(semant::ctx &ctx) override
        {
            return ctx.items[name].mut;
        }

        std::unique_ptr<hir::has_addr> to_hir_loc(semant::ctx &ctx) override
        {
            return std::make_unique<hir::mem_loc>(name);
        }

        std::unique_ptr<hir::value> to_hir(semant::ctx &ctx) override
        {
            return std::make_unique<hir::mem_loc>(name);
        }
    };

    struct assign_stmt : node
    {
        std::unique_ptr<lhs_node> lhs;
        std::unique_ptr<expr_node> rhs;

        assign_stmt(std::unique_ptr<lhs_node> lhs, std::unique_ptr<expr_node> rhs)
            : lhs{std::move(lhs)}, rhs{std::move(rhs)}
        { }

        std::unique_ptr<hir::instr> to_hir(semant::ctx &ctx, hir::function *fun) override
        {
            if (!lhs->mut(ctx)) {
                throw std::runtime_error("can't assign to non-mutable LHS");
            }
            return std::make_unique<hir::store>(
                lhs->to_hir_loc(ctx),
                rhs->to_hir(ctx)
            );
        }
    };

    struct if_stmt : node
    {
        std::unique_ptr<expr_node> test;
        std::vector<std::unique_ptr<node>> cons;
        std::vector<std::unique_ptr<node>> alt;

        if_stmt(std::unique_ptr<expr_node> test, std::vector<std::unique_ptr<node>> cons, std::vector<std::unique_ptr<node>> alt)
            : test{std::move(test)}, cons{std::move(cons)}, alt{std::move(alt)}
        { }

        std::unique_ptr<hir::instr> to_hir(semant::ctx &ctx, hir::function *fun) override
        {
            std::vector<std::unique_ptr<hir::block>> conditional_blocks;
            auto unconditional_block = std::make_unique<hir::block>();

            conditional_blocks.emplace_back(std::make_unique<hir::block>());

            conditional_blocks.back()->add_condition(test->to_hir(ctx));
            for (auto &&stmt : cons) {
                conditional_blocks.back()->emit(stmt->to_hir(ctx, fun));
            }
            for (auto &&stmt : alt) {
                unconditional_block->emit(stmt->to_hir(ctx, fun));
            }
            return std::make_unique<hir::conditional>(
                std::move(conditional_blocks),
                std::move(unconditional_block)
            );
        }
    };

    struct let_stmt : node
    {
        std::string name;
        std::unique_ptr<expr_node> expr;
        bool mut;

        let_stmt(const std::string &name, std::unique_ptr<expr_node> expr, bool mut)
            : name{name}, expr{std::move(expr)}, mut{mut}
        { }

        std::unique_ptr<hir::instr> to_hir(semant::ctx &ctx, hir::function *fun) override
        {
            fun->add_local(name, std::make_unique<semant::int_type>(64), mut);
            ctx.items[name] = {std::make_unique<semant::int_type>(64), mut};
            return std::make_unique<hir::store>(
                ident_exp{name}.to_hir_loc(ctx),
                expr->to_hir(ctx)
            );
        }
    };

    struct return_stmt : node
    {
        std::unique_ptr<expr_node> expr;

        return_stmt(std::unique_ptr<expr_node> expr) : expr{std::move(expr)} { }

        std::unique_ptr<hir::instr> to_hir(semant::ctx &ctx, hir::function *fun) override
        {
            return std::make_unique<hir::ret_val>(expr->to_hir(ctx));
        }
    };

    struct fun_def
    {
        std::string name;
        std::vector<std::unique_ptr<node>> body;

        fun_def(const std::string &name, std::vector<std::unique_ptr<node>> body)
            : name{name}, body{std::move(body)}
        { }

        std::unique_ptr<hir::function> to_hir(semant::ctx &ctx)
        {
            auto fun = std::make_unique<hir::function>(name);
            for (auto &&node : body) {
                fun->body.emit(node->to_hir(ctx, fun.get()));
            }
            return fun;
        }
    };
}

namespace finch::frontend
{
    enum class token_kind
    {
        unknown = -2,
        eof = -1,
        dedent = 0,
        indent,
        i64_lit,
        ident,
        kw_def,
        kw_let,
        kw_mut,
        kw_if,
        kw_elif,
        kw_else,
        kw_return,
        kw_true,
        kw_false,
        eq,
        assign,
        colon,
        lpar,
        rpar
    };

    struct token
    {
        token_kind kind;
        std::string lexeme;
        int64_t line;
        int64_t col;

        std::string to_string() const
        {
            switch (kind) {
                case token_kind::unknown:
                    return "unknown";
                case token_kind::eof:
                    return "eof";
                case token_kind::dedent:
                    return "dedent";
                case token_kind::indent:
                    return "indent";
                case token_kind::i64_lit:
                    return "i64_lit";
                case token_kind::ident:
                    return "ident";
                case token_kind::kw_def:
                    return "kw_def";
                case token_kind::kw_let:
                    return "kw_let";
                case token_kind::kw_mut:
                    return "kw_mut";
                case token_kind::kw_if:
                    return "kw_if";
                case token_kind::kw_elif:
                    return "kw_elif";
                case token_kind::kw_else:
                    return "kw_else";
                case token_kind::kw_return:
                    return "kw_return";
                case token_kind::kw_true:
                    return "kw_true";
                case token_kind::kw_false:
                    return "kw_false";
                case token_kind::eq:
                    return "eq";
                case token_kind::assign:
                    return "assign";
                case token_kind::colon:
                    return "colon";
                case token_kind::lpar:
                    return "lpar";
                case token_kind::rpar:
                    return "rpar";
            }
        }
    };

    class lexer
    {
        static std::unordered_map<std::string, token_kind> keywords;

        std::istream &in_;
        std::optional<token> peeked_;
        std::vector<int64_t> indents_ = { 0 };
        int pending_dedents_ = 0;
        bool new_line_ = true;
        int64_t line = 1;
        int64_t col = 1;

    public:
        lexer(std::istream &in) : in_{in} { }

        int next_char()
        {
            int c = in_.get();
            if (c == '\n') {
                line++;
                col = 1;
            } else {
                col++;
            }
            return c;
        }

        template<typename Pred>
        void read_while(std::string &acc, Pred pred)
        {
            while (!in_.eof() && pred(in_.peek())) {
                acc.push_back(next_char());
            }
        }

        token peek()
        {
            if (!peeked_) {
                peeked_ = next();
            }
            return peeked_.value();
        }

        token next()
        {
            static token tok{token_kind::unknown, ""};

            if (peeked_) {
                auto tok = peeked_.value();
                peeked_ = std::nullopt;
                return tok;
            }

            if (pending_dedents_ > 0) {
                pending_dedents_--;
                return {token_kind::dedent, "dedent"};
            }

            while (!in_.eof() && in_.peek() == '\n') {
                next_char();
                new_line_ = true;
            }

            auto indent = 0;
            while (!in_.eof() && isspace(in_.peek())) {
                next_char();
                if (new_line_) {
                    indent++;
                }
            }

            tok.line = line;
            tok.col = col;

            if (in_.eof() || new_line_) {
                new_line_ = false;

                if (indent > indents_.back()) {
                    indents_.push_back(indent);
                    return {token_kind::indent, "indent", tok.line, tok.col};
                }

                if (indent < indents_.back()) {
                    while (indent < indents_.back()) {
                        pending_dedents_++;
                        indents_.pop_back();
                    }
                    pending_dedents_--;
                    return {token_kind::dedent, "dedent", tok.line, tok.col};
                }
            }

            if (in_.eof()) {
                return {token_kind::eof, "end of file", tok.line, tok.col};
            }

            tok.lexeme.clear();

            if (isdigit(in_.peek())) {
                read_while(tok.lexeme, &isdigit);
                tok.kind = token_kind::i64_lit;
            } else if (isalnum(in_.peek())) {
                tok.kind = token_kind::ident;
                read_while(tok.lexeme, &isalnum);
                if (auto it = keywords.find(tok.lexeme); it != keywords.end()) {
                    tok.kind = it->second;
                }
            } else {
                switch (in_.peek()) {
                    case '=':
                        tok.lexeme.push_back(next_char());
                        tok.kind = token_kind::eq;
                        break;
                    case ':':
                        next_char();
                        if (in_.peek() == '=') {
                            next_char();
                            tok.lexeme = ":=";
                            tok.kind = token_kind::assign;
                        } else {
                            tok.lexeme = ":";
                            tok.kind = token_kind::colon;
                        }
                        break;
                    case '(':
                        tok.lexeme.push_back(next_char());
                        tok.kind = token_kind::lpar;
                        break;
                    case ')':
                        tok.lexeme.push_back(next_char());
                        tok.kind = token_kind::rpar;
                        break;
                    default:
                        read_while(tok.lexeme, std::not_fn(&isspace));
                        tok.kind = token_kind::unknown;
                        break;
                }
            }

            return tok;
        }
    };

    std::unordered_map<std::string, token_kind> lexer::keywords = {
        {"def", token_kind::kw_def},
        {"let", token_kind::kw_let},
        {"mut", token_kind::kw_mut},
        {"if", token_kind::kw_if},
        {"elif", token_kind::kw_elif},
        {"else", token_kind::kw_else},
        {"return", token_kind::kw_return},
        {"True", token_kind::kw_true},
        {"False", token_kind::kw_false},
    };

    class parser
    {
        lexer lex_;

    public:
        parser(std::istream &in) : lex_{in} { }

        token expect(token_kind expected, const std::string &desc)
        {
            auto tok = lex_.next();
            if (tok.kind != expected) {
                throw std::runtime_error{std::to_string(tok.line) + ", " + std::to_string(tok.col) + ": unexpected token; expecting " + desc + ", but got " + tok.to_string() + " (" + tok.lexeme + ") "};
            }
            return tok;
        }

        std::unique_ptr<ast::fun_def> parse()
        {
            expect(token_kind::kw_def, "`def` keyword");
            auto name = expect(token_kind::ident, "function name").lexeme;
            expect(token_kind::lpar, "(");
            expect(token_kind::rpar, ")");
            expect(token_kind::colon, ":");
            auto stmt = parse_suite();
            return std::make_unique<ast::fun_def>(name, std::move(stmt));
        }

        std::vector<std::unique_ptr<ast::node>> parse_suite()
        {
            expect(token_kind::indent, "indent");
            std::vector<std::unique_ptr<ast::node>> stmts;
            while (lex_.peek().kind > token_kind::dedent) {
                stmts.emplace_back(parse_stmt());
            }
            expect(token_kind::dedent, "dedent");
            return stmts;
        }

        std::unique_ptr<ast::node> parse_stmt()
        {
            switch (auto tok = lex_.peek(); tok.kind) {
                case token_kind::kw_if:
                    return parse_if_stmt();
                case token_kind::kw_let:
                    return parse_let_stmt();
                case token_kind::kw_return:
                    return parse_return_stmt();
                case token_kind::ident:
                    return parse_assign_or_call();
                default:
                    throw std::runtime_error{std::to_string(tok.line) + ", " + std::to_string(tok.col) + "syntax error: expecting statement"};
            }
        }

        std::unique_ptr<ast::node> parse_assign_or_call()
        {
            auto lhs = parse_lhs();
            expect(token_kind::assign, ":=");
            auto rhs = parse_expr();
            return std::make_unique<ast::assign_stmt>(std::move(lhs), std::move(rhs));
        }

        std::unique_ptr<ast::node> parse_let_stmt()
        {
            expect(token_kind::kw_let, "`let` keyword");
            auto mut = false;
            if (lex_.peek().kind == token_kind::kw_mut) {
                lex_.next();
                mut = true;
            }
            auto name = expect(token_kind::ident, "variable name").lexeme;
            expect(token_kind::eq, "=");
            auto expr = parse_expr();
            return std::make_unique<ast::let_stmt>(name, std::move(expr), mut);
        }
        
        std::unique_ptr<ast::node> parse_if_stmt()
        {
            expect(token_kind::kw_if, "`if` keyword");
            auto test = parse_expr();
            expect(token_kind::colon, ":");
            auto cons = parse_suite();
            expect(token_kind::kw_else, "`else` keyword");
            expect(token_kind::colon, ":");
            auto alt = parse_suite();
            return std::make_unique<ast::if_stmt>(std::move(test), std::move(cons), std::move(alt));
        }

        std::unique_ptr<ast::node> parse_return_stmt()
        {
            expect(token_kind::kw_return, "`return` keyword");
            auto expr = parse_expr();
            return std::make_unique<ast::return_stmt>(std::move(expr));
        }

        std::unique_ptr<ast::lhs_node> parse_lhs()
        {
            switch (auto tok = lex_.peek(); tok.kind) {
                case token_kind::ident:
                    return std::make_unique<ast::ident_exp>(lex_.next().lexeme);
                default:
                    throw std::runtime_error{std::to_string(tok.line) + ", " + std::to_string(tok.col) + ": expecting LHS"};
            }
        }

        std::unique_ptr<ast::expr_node> parse_expr()
        {
            switch (auto tok = lex_.peek(); tok.kind) {
                case token_kind::ident:
                    return std::make_unique<ast::ident_exp>(lex_.next().lexeme);
                case token_kind::kw_true:
                    lex_.next();
                    return std::make_unique<ast::bool_lit_exp>(true);
                case token_kind::kw_false:
                    lex_.next();
                    return std::make_unique<ast::bool_lit_exp>(false);
                case token_kind::i64_lit: {
                    auto lit = expect(token_kind::i64_lit, "i64 literal").lexeme;
                    return std::make_unique<ast::int_lit_exp>(std::stol(lit), true);
                }
                default:
                    throw std::runtime_error{std::to_string(tok.line) + ", " + std::to_string(tok.col) + ": expecting expression"};
            }
        }
    };
}

int main(int argc, char **argv)
{
    std::ifstream in{argv[1]};
    finch::frontend::parser parser{in};
    auto fun_ast = parser.parse();

    finch::semant::ctx semant_ctx;
    auto fun_hir = fun_ast->to_hir(semant_ctx);

    finch::backend::codegen::compiler_ctx compiler_ctx;
    compiler_ctx.init(argv[1]);

    fun_hir->codegen(compiler_ctx);
    compiler_ctx.llvm_module->print(llvm::errs(), nullptr);

    return 0;
}
